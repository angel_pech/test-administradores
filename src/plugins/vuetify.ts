import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';
import { es } from 'vuetify/src/locale';

Vue.use(Vuetify);

export default new Vuetify({
	theme: {
		themes: {
			light: {
				primary: '#4FB9BB',
				secondary: '#424242',
				accent: '#82B1FF',
				error: '#FF5252',
				info: '#2196F3',
				success: '#4CAF50',
				warning: '#FFC107',
			},
			dark: {
				primary: '#4FB9BB',
			},
		},
	},
	lang: {
		locales: { es },
		current: 'es',
	},
});
