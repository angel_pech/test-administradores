import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
	{
		path: '/',
		name: 'Home',
		redirect: {
			name: 'Admins',
		},
		component: () => import('@/views/modules/Index.vue'),
		children: [
			{
				path: '/admins',
				name: 'Admins',
				component: () => import('@/views/modules/admins/Index.vue'),
			},
			{
				path: '/admins/create',
				name: 'CreateAdmins',
				component: () => import('@/views/modules/admins/create/Index.vue'),
			},
			{
				path: '/admins/:id',
				name: 'ReadAdmins',
				component: () => import('@/views/modules/admins/read/Index.vue'),
			},
			{
				path: '/admins/:id/edit',
				name: 'EditAdmins',
				component: () => import('@/views/modules/admins/edit/Index.vue'),
			},
			{
				path: '/admins/success',
				name: 'SuccessAdmins',
				component: () => import('@/views/modules/admins/success/Index.vue'),
			},
			{
				path: '/catalogs',
				name: 'Catalogs',
				component: () => import('@/views/modules/catalogs/Index.vue'),
			},
			{
				path: '/leaders',
				name: 'Leaders',
				component: () => import('@/views/modules/leaders/Index.vue'),
			},
			{
				path: '/users',
				name: 'Users',
				component: () => import('@/views/modules/users/Index.vue'),
			},
			{
				path: '/my-business',
				name: 'MyBusiness',
				component: () => import('@/views/modules/my-business/Index.vue'),
			},
			{
				path: '/courses',
				name: 'Courses',
				component: () => import('@/views/modules/courses/Index.vue'),
			},
			{
				path: '/rewards',
				name: 'Rewards',
				component: () => import('@/views/modules/rewards/Index.vue'),
			},
			{
				path: '/events',
				name: 'Events',
				component: () => import('@/views/modules/events/Index.vue'),
			},
			{
				path: '/notifications',
				name: 'Notifications',
				component: () => import('@/views/modules/notifications/Index.vue'),
			},
			{
				path: '/reports',
				name: 'Reports',
				component: () => import('@/views/modules/reports/Index.vue'),
			},
		],
	},
];

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes,
});

export default router;
